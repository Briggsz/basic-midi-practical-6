/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.setDefaultMidiOutput("simpleSynth virtual input");
    
    addAndMakeVisible(midiLabel);
    
    messageType.addItem("Note", 1);
    messageType.addItem("Control", 2);
    messageType.addItem("Program", 3);
    messageType.addItem("Pitch Bend", 4);
    addAndMakeVisible(messageType);
    
    midiChannelSlider.setSliderStyle(Slider::IncDecButtons);
    addAndMakeVisible(midiChannelSlider);
    
    midiNumberSlider.setSliderStyle(Slider::IncDecButtons);
    addAndMakeVisible(midiNumberSlider);
    
    midiVeloSlider.setSliderStyle(Slider::IncDecButtons);
    addAndMakeVisible(midiVeloSlider);
    
    sendMidi.setButtonText("Send Midi");
    addAndMakeVisible(sendMidi);
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    DBG("Get Width "<< getWidth()<<" GetHeight"<< getHeight());
    
    midiLabel.setBounds(10, 10, getWidth() - 20, 40);
    
    messageType.setBounds(10, 60, 125, 40);
    
    midiChannelSlider.setBounds(145, 60, 100, 40);
    midiNumberSlider.setBounds(255, 60, 100, 40);
    midiVeloSlider.setBounds(365, 60, 100, 40);
    
    sendMidi.setBounds(10, 110, getWidth() - 20, 40);
}

void MainComponent::handleIncomingMidiMessage (MidiInput* source,const MidiMessage& message)
{
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    String MidiText;
    
    DBG("Message Input Recieved");
    if (message.isNoteOnOrOff())
    {
        MidiText << "Note On : Channel " << message.getChannel() << "\n";
        MidiText << "Nuber : " <<message.getNoteNumber()<< "\n";
        MidiText << "Velocity : " << message.getVelocity();
    }
    
    midiLabel.getTextValue()= MidiText;
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    
}

